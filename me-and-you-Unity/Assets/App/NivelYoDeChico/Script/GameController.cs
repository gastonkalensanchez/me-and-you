using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    public int width = 10; // Ancho del laberinto
    public int height = 10; // Altura del laberinto
    public GameObject wallPrefab; // Prefabricado de la pared
    public float wallWidth = 1f; // Ancho de la pared
    public float wallHeight = 1f; // Altura de la pared

    private bool[,] visited; // Matriz para rastrear celdas visitadas
    private List<Vector2Int> walls = new List<Vector2Int>(); // Lista de paredes

    private void Start()
    {
        GenerateMaze();
    }

    // Funci�n para generar el laberinto
    private void GenerateMaze()
    {
        visited = new bool[width, height]; // Inicializar matriz visitada
        for (int i = 0; i < width; i++)
        {
            for (int j = 0; j < height; j++)
            {
                visited[i, j] = false; // Marcar todas las celdas como no visitadas
            }
        }

        // Comenzar desde una celda aleatoria
        int startX = Random.Range(0, width);
        int startY = Random.Range(0, height);
        VisitCell(startX, startY);

        // Algoritmo de Prim
        while (walls.Count > 0)
        {
            int randomIndex = Random.Range(0, walls.Count); // Elegir una pared aleatoria
            Vector2Int wall = walls[randomIndex];
            walls.RemoveAt(randomIndex);

            // Comprobar si la pared divide celdas visitadas y no visitadas
            int x = wall.x;
            int y = wall.y;
            if ((x % 2 == 0 && visited[x + 1, y] && !visited[x - 1, y]) ||
                (y % 2 == 0 && visited[x, y + 1] && !visited[x, y - 1]))
            {
                // Destruir la pared
                DestroyWall(x, y);
                VisitCell(x, y);
            }
        }
    }

    // Funci�n para marcar una celda como visitada y agregar las paredes vecinas a la lista
    private void VisitCell(int x, int y)
    {
        visited[x, y] = true;
        if (x >= 2 && !visited[x - 2, y]) walls.Add(new Vector2Int(x - 1, y));
        if (x < width - 2 && !visited[x + 2, y]) walls.Add(new Vector2Int(x + 1, y));
        if (y >= 2 && !visited[x, y - 2]) walls.Add(new Vector2Int(x, y - 1));
        if (y < height - 2 && !visited[x, y + 2]) walls.Add(new Vector2Int(x, y + 1));
    }

    // Funci�n para destruir una pared
    private void DestroyWall(int x, int y)
    {
        float xPos = x - width / 2f + 0.5f;
        float yPos = y - height / 2f + 0.5f;

        Vector3 position = new Vector3(xPos, 0, yPos);
        GameObject wall = Instantiate(wallPrefab, position, Quaternion.identity);
        wall.transform.localScale = new Vector3(wallWidth, wallHeight, 1f);
    }
}
