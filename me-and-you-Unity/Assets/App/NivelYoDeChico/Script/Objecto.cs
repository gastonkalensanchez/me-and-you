using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;

public class Objecto : MonoBehaviour
{
    Rigidbody rb;
    Vector3 scale;
    void Awake()
    {
        rb = GetComponent<Rigidbody>();
        scale = transform.localScale;
    }

    public void Agarrado()
    {
        rb.constraints = RigidbodyConstraints.FreezeAll;
        transform.localScale = new Vector3(0.2f, 0.2f, 0.2f);       
    }
    public void Suelto()
    {
        StartCoroutine(WaitAndStop(20));
        transform.localScale = scale;
    }
    private IEnumerator WaitAndStop(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        rb.constraints = RigidbodyConstraints.None;
    }

}
