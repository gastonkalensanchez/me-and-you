using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class JugadorController : MonoBehaviour
{
    public TMP_Text interactuar;
    float speed = 5f; 
    float sensitivity = 5f; 
    private Rigidbody rb;
    private Camera playerCamera; 
    private Vector3 rotation = Vector3.zero;
    bool l1, l2, l3;
    public int partes1 = 0,partes3 = 0;
    public TMP_Text textoparte;
    public bool lvl1;
    private void Start()
    {
        rb = GetComponent<Rigidbody>();
        playerCamera = GetComponentInChildren<Camera>();

        interactuar.gameObject.SetActive(false);
    }

    private void FixedUpdate()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");
        Vector3 movement = new Vector3(moveHorizontal, 0f, moveVertical);
        movement.Normalize();
        rb.MovePosition(rb.position + transform.TransformDirection(movement) * speed * Time.fixedDeltaTime);
        float mouseX = Input.GetAxis("Mouse X") * sensitivity;
        float mouseY = Input.GetAxis("Mouse Y") * sensitivity;
        rotation.x -= mouseY;
        rotation.x = Mathf.Clamp(rotation.x, -90f, 90f); 
        rotation.y += mouseX;
        playerCamera.transform.localRotation = Quaternion.Euler(rotation.x, 0f, 0f);
        transform.eulerAngles = new Vector3(0f, rotation.y, 0f);
    }
    void Update()
    {

        if (lvl1 && textoparte != null)
        textoparte.text = partes1.ToString() + "/3";
        else if (textoparte != null)
        textoparte.text = partes3.ToString() + "/3";
        if (Input.GetKeyDown(KeyCode.E))
        {
            if (l1)
            {
                SceneManager.LoadScene("VideosLVL1");
            }
            if (l2)
            {
                SceneManager.LoadScene("VideosLVL2");
            }
            if (l3)
            {
                SceneManager.LoadScene("VideosLVL3");
            }

        }
        if (partes1 >= 3)
        {
            SceneManager.LoadScene("VideosLVL1FINAL");
        }
        if (partes3 >= 3)
        {
            SceneManager.LoadScene("VideosLVL3FINAL");
        }


    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("ParteLvl1"))
        {
            partes1 += 1;
            Destroy(other.gameObject);
        }
        if (other.gameObject.CompareTag("ParteLvl3"))
        {
            partes3 += 1;
            Destroy(other.gameObject);
        }

    }
    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.CompareTag("Expo1"))
        {
            interactuar.gameObject.SetActive(true);
            l1 = true;

        }
        if (other.gameObject.CompareTag("Expo2"))
        {
            interactuar.gameObject.SetActive(true);
            l2 = true;
        }
        if (other.gameObject.CompareTag("Expo3"))
        {
            interactuar.gameObject.SetActive(true);
            l3 = true;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Expo1"))
        {
            interactuar.gameObject.SetActive(false);
            l1 = false;
        }
        if (other.gameObject.CompareTag("Expo2"))
        {
            interactuar.gameObject.SetActive(false);
            l2 = false;
        }
        if (other.gameObject.CompareTag("Expo3"))
        {
            interactuar.gameObject.SetActive(false);
            l3 = false;
        }
    }

}
