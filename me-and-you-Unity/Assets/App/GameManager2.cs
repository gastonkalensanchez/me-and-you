using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager2 : MonoBehaviour
{
    public List<Transform> spawnLocations; // List of transform positions
    public List<GameObject> prefabsToSpawn; // List of prefabs to instantiate
                                            // Start is called before the first frame update
    private float lastSpawnTime = 0f; // Time of the last prefab instantiation
    public float spawnCooldown = 2f; // Cooldown period in seconds
    public static GameManager2 instancia3;
    public int parte3 = 0;
    public GameObject parte;
    public Transform transformparte;
    void Start()
    {
        if (instancia3 == null)
        {
            instancia3 = this;
        }

    }

    // Update is called once per frame
    void Update()
    {
        if (Time.time - lastSpawnTime >= spawnCooldown)
        {
            SpawnPrefab();
            lastSpawnTime = Time.time; // Update last spawn time
        }
        if (parte3 >=3)
        {
            GameObject p = Instantiate(parte,transformparte.position,Quaternion.identity);
            parte3 = 0;
        }
    }
    private void SpawnPrefab()
    {

        if (spawnLocations.Count == 0 || prefabsToSpawn.Count == 0)
        {        
            return;
        }
        int randomPrefabIndex = Random.Range(0, prefabsToSpawn.Count);
        int randomSpawnLocationIndex = Random.Range(0, spawnLocations.Count);

        GameObject prefabInstance = Instantiate(prefabsToSpawn[randomPrefabIndex], spawnLocations[randomSpawnLocationIndex].position, Quaternion.identity);

    }
}
