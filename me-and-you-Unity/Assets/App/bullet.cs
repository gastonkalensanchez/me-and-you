using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bullet : MonoBehaviour
{
    public float speed = 5000f;
    public float lifetime = 800f;

    private void Start()
    {
        Destroy(gameObject, lifetime);
    }

    private void Update()
    {
        transform.Translate(Vector3.forward * speed * Time.deltaTime);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("recuerdo"))
        {
            GameManager2.instancia3.parte3 += 1;
            Destroy(other.gameObject);
            Destroy(gameObject);
        }
    }
}
