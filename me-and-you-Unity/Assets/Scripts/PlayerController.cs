using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{
    float velocidad = 5;

    public int partes2 = 0;
    public TMP_Text texto;
    void Update()
    {
        float x = Input.GetAxis("Horizontal");
        float y = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(x, 0, y);
        texto.text = partes2.ToString() + "/3";
        transform.Translate(movement * velocidad * Time.deltaTime);
        if (partes2 >= 3)
        {
            SceneManager.LoadScene("VideosLVL2FINAL");
        }
    }
    private void OnTriggerEnter(Collider other)
    {

        if (other.gameObject.CompareTag("ParteLvl2"))
        {
            partes2 += 1;
            Destroy(other.gameObject);
        }

    }

}
