using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPC_BaseState
{
    protected string Name { get; set; }
    protected StateMachine sm;
    public float distance { get; set; }
    public NPC_BaseState(string name,StateMachine SM)
    {
        Name = name;
        sm = SM;
    }

    public virtual void Iniciate(bool infected)
    {

    }

    public virtual void Refresh()
    {

    }

    public virtual void Finish()
    {

    }
}
