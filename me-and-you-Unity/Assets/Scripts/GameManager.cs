using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;

    public List<GameObject> NotInfected = new List<GameObject>();

    public GameObject txtGameOver;

    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }
    }

    public void GameOver()
    {
        txtGameOver.SetActive(true);
        SceneManager.LoadScene("Lobby");
    }

}
