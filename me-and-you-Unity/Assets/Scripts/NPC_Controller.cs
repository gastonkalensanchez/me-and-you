using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class NPC_Controller : StateMachine
{
    public Material matInfected;
    public Material matAttacking;
    public Material matNotInfected;

    [HideInInspector] public NavMeshAgent navMeshAgent;

    public bool infected;

    NPC_IdleState idleState;
    NPC_AttackState attackState;

    NPC_BaseState currentState;

    private void Awake()
    {
        navMeshAgent = this.gameObject.GetComponent<NavMeshAgent>();
        idleState = new NPC_IdleState("Idle", this);
        attackState = new NPC_AttackState("Attack", this);

        currentState = idleState;
        Initiate();
    }

    private void Update()
    {
        Refresh();
    }
    public void Initiate()
    {
        if (currentState != null)
        {
            currentState.Iniciate(infected);
        }
    }
    public void Refresh()
    {
        if(currentState != null)
        {
            currentState.Refresh();
        }
    }

    public void Infected()
    {
        infected = true;
        GameManager.instance.NotInfected.Remove(this.gameObject);
        SwitchToIdleState();
    }

    public void SwitchToIdleState()
    {
        currentState.Finish();
        currentState = null;
        currentState = idleState;
        Initiate();
    }
    public void SwitchToAttackState()
    {
        currentState.Finish();
        currentState = null;
        currentState = attackState;
        Initiate();
    }

}
