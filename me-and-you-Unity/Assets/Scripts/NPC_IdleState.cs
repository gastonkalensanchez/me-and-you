using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPC_IdleState : NPC_BaseState
{
    public NPC_IdleState(string name,NPC_Controller controller) : base(name, controller) { }

    public bool Infected = false;
    float x, z;
    public override void Iniciate(bool infected)
    {
        switch(infected)
        {
            case (true):
                sm.GetComponent<Renderer>().material = ((NPC_Controller)sm).matInfected;
                break;
            case (false):
                sm.GetComponent<Renderer>().material = ((NPC_Controller)sm).matNotInfected;
                break;
        }
        Infected = infected;
        x = ((NPC_Controller)sm).transform.position.x;
        z = ((NPC_Controller)sm).transform.position.z;
    }

    public override void Refresh()
    {
        if(((NPC_Controller)sm).transform.position.x == x && ((NPC_Controller)sm).transform.position.z == z)
        {
            x = Random.Range(-11, 11);
            z = Random.Range(-8, 8);
        }
        ((NPC_Controller)sm).navMeshAgent.SetDestination(new Vector3(x, 1, z));

        if(Infected)
        {
            GetClosestNotInfected();
            if (distance < 3)
            {
                ((NPC_Controller)sm).SwitchToAttackState();
            }
        }
    }

    public override void Finish()
    {
        base.Finish();
    }

    public GameObject GetClosestNotInfected()
    {
        float distanceToClosest = int.MaxValue;
        GameObject gameObject = null;
        foreach(GameObject go in GameManager.instance.NotInfected)
        {
            if(Vector3.Distance(sm.gameObject.transform.position,go.transform.position) < distanceToClosest)
            {
                distanceToClosest = Vector3.Distance(sm.gameObject.transform.position, go.transform.position);
                gameObject = null;
                gameObject = go;
            }
        }
        distance = distanceToClosest;
        return gameObject;
    }
}
